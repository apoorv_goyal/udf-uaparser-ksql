package udf_ksql_user_agent_parser;

import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.jsonschema.JsonSchema;
import com.fasterxml.jackson.databind.node.JsonNodeCreator;
import com.github.fge.jackson.JsonNodeReader;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

import org.apache.kafka.common.Configurable;

import io.confluent.ksql.function.udf.Udf;
import io.confluent.ksql.function.udf.UdfDescription;
import io.confluent.ksql.function.udf.UdfParameter;


@UdfDescription(name = "check_json", author = "user", version = "1.0.0", description = "A custom formula for important business logic.")
public class CheckJSON implements Configurable {
   
    @Override
    public void configure(Map<String, ?> arg0) {
        String baseValue= (String) arg0.get("ksql.functions.formula.base.value");
        
    }
    // @Udf(description = "The standard version of the formula with integer parameters.")
    // public String checkjsonString(@UdfParameter String column) {
        
    // }
   
}

